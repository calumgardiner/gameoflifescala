package gameoflife.board

object CellStatus extends Enumeration {
  type CellStatus = Boolean;

  val alive = true;
  var dead = false;

}