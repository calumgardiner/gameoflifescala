package gameoflife.board
/**
 * GameBoard model of the Game of Life will hold the current status of the cells
 */
class GameBoard {

  var xSize = 0;
  var ySize = 0;
  var board: Array[Array[Cell]] = Array.empty;

  def this(xSize: Int, ySize: Int) = {
    this();
    this.xSize = xSize;
    this.ySize = ySize;
    this.board = Array.ofDim[Cell](xSize, ySize);
    for (x <- 0 until xSize) {
      for (y <- 0 until ySize) {
        board(x)(y) = new Cell(false)
      }
    }
  }

  def giveLife(x: Int, y: Int) {
    this.board(x)(y).giveLife;
  }

  def kill(x: Int, y: Int) {
    this.board(x)(y).kill;
  }

}