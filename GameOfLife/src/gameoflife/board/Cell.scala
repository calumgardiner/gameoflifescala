package gameoflife.board
/**
 * A living or dead cell within the game of life.
 */
class Cell {

  var status = CellStatus.dead;

  def this(status: Boolean) = {
    this();
    if (status) giveLife else kill;
  }

  def giveLife {
    status = CellStatus.alive;
  }

  def kill {
    status = CellStatus.dead;
  }
}