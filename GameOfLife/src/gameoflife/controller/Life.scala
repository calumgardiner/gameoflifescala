package gameoflife.controller

import gameoflife.board.GameBoard
import gameoflife.board.Cell
import gameoflife.board.CellStatus

/**
 * Life, GameOfLife controller class, takes a board and works out the ticks.
 */
class Life {

  var gameBoard: GameBoard = null;
  var running = true;

  def this(gameBoard: GameBoard) = {
    this();
    this.gameBoard = gameBoard;
  }

  def stopRunning {
    running = false;
  }

  def startRunning {
    running = true;
  }

  /**
   * Do 1 tick of life for the entire board.
   */
  def tick {
    if (running) {
      // Copy board cells so calculation is done on previous iteration
      var cells = gameBoard.board.clone();
      for (x <- 0 until gameBoard.xSize) {
        for (y <- 0 until gameBoard.ySize) {
          var currentCell = cells(x)(y);
          var neighbours = getNeighbours(x, y, cells);
          var aliveNeighbours = neighbours.count(_.status == CellStatus.alive);
          aliveNeighbours match {
            case x if x < 2 => gameBoard.kill(x, y);
            case 2 => if (currentCell.status == CellStatus.alive) gameBoard.giveLife(x, y);
            case 3 => gameBoard.giveLife(x, y);
            case x if x > 3 => gameBoard.kill(x, y);
          }
        }
      }
    }
  }

  /**
   * Get the neighbours (1 cell adjacent) of the x,y cell.
   */
  def getNeighbours(x: Int, y: Int, cells: Array[Array[Cell]]) = {
    var width = gameBoard.xSize;
    var height = gameBoard.ySize;
    var xminus1 = if (x == 0) width - 1 else x - 1;
    var yminus1 = if (y == 0) height - 1 else y - 1;
    var xplus1 = if (x == (width - 1)) 0 else x + 1;
    var yplus1 = if (y == (height - 1)) 0 else y + 1;
    Array(cells(xminus1)(yminus1),
      cells(x)(yminus1),
      cells(xplus1)(yminus1),
      cells(xminus1)(y),
      cells(xplus1)(y),
      cells(xminus1)(yplus1),
      cells(x)(yplus1),
      cells(xplus1)(yplus1));
  }

}