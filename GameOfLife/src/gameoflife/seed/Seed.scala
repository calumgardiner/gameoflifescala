package gameoflife.seed

import gameoflife.board.GameBoard

trait Seed {

  var name: String;
  
  def seedBoard(board: GameBoard);

}