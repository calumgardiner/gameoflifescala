package gameoflife.seed

import gameoflife.board.GameBoard

class RandomSeed extends Seed {

  var name = "Random Seed";

  def seedBoard(board: GameBoard) {
    for (x <- 0 until board.xSize) {
      for (y <- 0 until board.ySize) {
        if (math.random < 0.5) board.giveLife(x, y) else board.kill(x, y);
      }
    }
  }
}