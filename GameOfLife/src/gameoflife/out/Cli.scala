package gameoflife.out

import gameoflife.seed.Seed
import gameoflife.seed.RandomSeed
import gameoflife.board.GameBoard
import gameoflife.controller.Life
import gameoflife.board.CellStatus

object Cli {

  var seeds: Array[Seed] = Array(new RandomSeed);
  var board = new GameBoard(40, 40);
  var life = new Life(board);
  seeds(0).seedBoard(board);

  def main(args: Array[String]): Unit = {
    while (true) {
      Cls
      println(createCliBoard(board));
      life.tick;
      Thread.sleep(100);
      return;
    }
  }

  def createCliBoard(board: GameBoard) = {
    var string = "";
    for (y <- 0 until board.ySize) {
      for (x <- 0 until board.xSize) {
        if (board.board(x)(y).status == CellStatus.alive)
          string += "[#]"
        else
          string += "[ ]";
      }
      string += "\n";
    }
    string;
  }

}
object Cls extends App { print("\033[2J") }